<h1 align="center">WEBSITE LINK</h1>
<p align="center">https://tatarualexandru777.gitlab.io/website-portfolio/</p>

<h1 align="center">DESCRIPTION</h1>
<p align="center">This is my first project related to web development. It's my personal portfolio with some of my work i have done to showcase my skills in digital art, 3D modelling and software development. The website is built with HTML, CSS and JavaScript. The design was made in Adobe XD. 

- Developed a modal where the user can select one of my projects and see all the details that are stored in a JSON file. 
- Made the portfolio page where the user can filter which area of my work he wants to see.
- Created the contact page where the user can send me an email through my website.

</p>

<h1 align="center">BUILT WITH</h1>
WebStorm(IDE), GitLab, JavaScript, HTML5,  CSS, JSON

<h1 align="center">SCREENSHOTS</h1>
 <img src="https://i.imgur.com/cRT4f0J.png" align="center">
   
