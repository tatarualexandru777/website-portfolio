var digitalArtData = [];
var softwareData = [];
var modellingData = [];

window.onload = function (){
    "use strict";
    readTextFile("../data/digitalArt.json", function(text){
        digitalArtData = JSON.parse(text);
    });
    readTextFile("../data/software.json", function (text){
        softwareData = JSON.parse(text);
    });
    readTextFile("../data/modelling.json", function (text){
        modellingData = JSON.parse(text);
    });
}

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

function changeNavigationItemStyle(element) {
    "use strict";
    var navigationItems = element.parentElement.children;

    for(var i = 0; i < navigationItems.length; i++){
        navigationItems[i].className = "navigation-item";
    }
    element.className = "selected";
}

function onFilterChanged(index, element) {
    "use strict";
    var images = document.getElementsByClassName("portfolio-page-body")[0].children;
    changeNavigationItemStyle(element);

    for (var i = 0; i < images.length; i++) {
        switch (index) {
            case 0 :
                images[i].style.display = "block";
                break;
            case 1 :
                images[i].style.display = images[i].className === "digital-art" ? "block" : "none";
                break;
            case 2 :
                images[i].style.display = images[i].className === "software" ? "block" : "none";
                break;
            case 3 :
                images[i].style.display = images[i].className === "3d-modelling" ? "block" : "none";
                break;
            default :
                return;
        }
    }
}

function showModal(index, categoryId) {
    "use strict";
    var showModal = document.querySelector('.portfolio-page-modal');
    var showOverlay = document.querySelector('.overlay');
    var imageElement = document.querySelector('.project-image');
    var titleElement = document.querySelector('.project-title');
    var infoElement = document.querySelector('.project-info');
    var linkElement = document.querySelector('.project-link');

    showModal.style.display = "grid";
    showOverlay.style.display = "block";
    linkElement.innerHTML = "";
    linkElement.href = "";

    switch (categoryId) {
        case 0:
            imageElement.src = digitalArtData[index].image;
            titleElement.innerHTML = digitalArtData[index].title;
            infoElement.innerHTML = digitalArtData[index].info;
            break;
        case 1:
            imageElement.src = softwareData[index].image;
            titleElement.innerHTML = softwareData[index].title;
            infoElement.innerHTML = softwareData[index].info;
            linkElement.href = softwareData[index].link;
            linkElement.innerHTML = softwareData[index].visit;
            break;
        case 2:
            imageElement.src = modellingData[index].image;
            titleElement.innerHTML = modellingData[index].title;
            infoElement.innerHTML = modellingData[index].info;
    }
}

function closeModal() {
    "use strict";
    var close = document.querySelector('.portfolio-page-modal');
    var closeOverlay = document.querySelector('.overlay')
    close.style.display = "none";
    closeOverlay.style.display = "none";
}
