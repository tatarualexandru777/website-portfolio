window.onload = function (){
    "use strict";
    setUpForm();
}

function setUpForm() {
    "use strict";
    var form = document.getElementById('form');

    form.onsubmit = sendData;
}

function sendData(event) {
    "use strict";
    var userName = document.getElementById('name').value;
    var message = document.getElementById('form-message').value;
    var title = document.getElementById('title').value;

    event.preventDefault();
    window.open('mailto:tatarualexandru777@gmail.com?subject='+ title+'. From '+userName+'&body='+message);
}

function checkValid() {
    "use strict";
    var isValid = document.getElementById('form').checkValidity();
    document.getElementById('submitButton').disabled = !isValid;
}